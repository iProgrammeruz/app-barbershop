package uz.barbershop.bot.mister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MisterApplication {

    public static void main(String[] args) {

        SpringApplication.run(MisterApplication.class, args);
    }

}
