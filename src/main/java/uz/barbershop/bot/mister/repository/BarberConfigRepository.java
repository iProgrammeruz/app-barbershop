package uz.barbershop.bot.mister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.barbershop.bot.mister.entity.BarberConfigs;
import uz.barbershop.bot.mister.entity.Users;

import java.util.Optional;
import java.util.UUID;

public interface BarberConfigRepository extends JpaRepository<BarberConfigs, UUID> {

    Optional<BarberConfigs> findByUser(Users user);

}
