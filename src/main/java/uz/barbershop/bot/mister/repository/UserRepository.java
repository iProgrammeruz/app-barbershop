package uz.barbershop.bot.mister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.barbershop.bot.mister.entity.enums.Roles;
import uz.barbershop.bot.mister.entity.Users;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<Users, UUID> {

    Optional<Users> findByPhoneNumber(String phoneNumber);

    Optional<Users> findByChatId(Long chatId);

    Optional<Users> findByUsername(String username);

    List<Users> findAllByRole(Roles role);

}
