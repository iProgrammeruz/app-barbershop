package uz.barbershop.bot.mister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.barbershop.bot.mister.entity.Days;

import java.util.UUID;

@Repository
public interface DaysRepository extends JpaRepository<Days, UUID> {



}
