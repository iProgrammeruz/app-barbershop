package uz.barbershop.bot.mister.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.barbershop.bot.mister.entity.enums.OrderStatus;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "orders")
public class Orders extends AbsEntity {

    @ManyToOne
    private Users user;

    @ManyToOne
    private Users barber;

    @ManyToOne
    private Days day;

    @ManyToOne
    private Hours hour;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

}
