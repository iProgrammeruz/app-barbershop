package uz.barbershop.bot.mister.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "hours")
public class Hours extends AbsEntity {

    @Column
    private String name;

    @Column
    private Integer startHour; // 9
    @Column
    private Integer startMinute; // 0

    @Column
    private Integer endHour; // 20
    @Column
    private Integer endMinute; // 0

    @Column
    private Boolean isOpen;

    @Column
    private Boolean isOrdered;

    @ManyToOne(fetch = FetchType.LAZY)
    private Users user;

    @ManyToOne
    private Days day;

}
